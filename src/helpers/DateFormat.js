export default class DateFormat {

  static toString(date) {

      const day   = date.getDate()
      const mouth = date.getMonth() < 9 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1
      const year  = date.getFullYear()
    
      return `${day}/${mouth}/${year}`
      
    }
  
}