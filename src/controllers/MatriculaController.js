import inputMessageError from '../services/inputMessageError'
import Matricula from '../models/Matricula'
import ListaMatricula from '../models/ListaMatricula'
import MatriculaComponent from '../components/MatriculaComponent'

export default class MatriculaController {

  constructor() {
    
    inputMessageError()

    this._lista  = document.querySelector('.lista tbody')
    this._nome   = document.querySelector('.nome')
    this._idade  = document.querySelector('.idade')
    this._altura = document.querySelector('.altura')
    this._plano  = document.querySelector('.plano')

    this._listaMatricula = new ListaMatricula()
    this._matriculaComponent = new MatriculaComponent( document.querySelector('#matriculaComponent') )
    this._matriculaComponent.update(this._listaMatricula)
  }

  adiciona(event) {

    event.preventDefault()
    
    const matricula = new Matricula(this._nome.value, this._idade.value, this._altura.value, this._plano.value)
    this._listaMatricula.adiciona(matricula)
    this._matriculaComponent.update(this._listaMatricula)

    this._limpaCampos()
  }

  _limpaCampos() {

    this._nome.value = ''
    this._idade.value = ''
    this._altura.value = ''
    this._plano.options[0].selected = true
  }

}