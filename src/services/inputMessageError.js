export default () => {
  const inputs = document.querySelectorAll('input')
  const errorValidate = (event, message = '') => event.target.setCustomValidity(message)

  inputs.forEach(input => {

    input.addEventListener('invalid', event => errorValidate(event, 'Preencha o campo'))
    input.addEventListener('keypress', event => errorValidate(event))

  })
}