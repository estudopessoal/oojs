export default class Component {
  constructor(element) {
    this._element = element
  }

  _render() {
    return ``
  }

  update(model) {
    this._element.innerHTML = this._render(model)
  }
}