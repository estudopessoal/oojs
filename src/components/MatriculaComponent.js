import Component from './Component'
export default class MatriculaComponent extends Component {

  _render(matriculaLista) {
    return `
      <table class="lista">
        <thead>
          <tr>
            <th>Data</th>
            <th>Nome</th>
            <th>Idade</th>
            <th>Altura (cm)</th>
            <th>Plano</th>
          </tr>
        </thead>
        <tbody>
        
          ${matriculaLista.lista.length === 0 ? `

            <tr>
              <td colspan="5" class="lista__vazia">
                Nenhuma matricula cadastrada
              </td>
            </tr>

          `: ``}

          ${matriculaLista.lista.map(matricula => `

            <tr>
              <td>${matricula.data}</td>
              <td>${matricula.nome}</td>
              <td>${matricula.idade}</td>
              <td>${matricula.altura}</td>
              <td>${matricula.plano}</td>
            </tr>
            
          `).join('')}

        </tbody>
      </table>    
    `
  }

}