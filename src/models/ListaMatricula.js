export default class ListaMatricula {

  constructor() {
    this._lista = []
  }

  adiciona(matricula) {
    this._lista.push(matricula)
  }

  get lista() {
    return [...this._lista]
  }

}