import DateFormat from '../helpers/DateFormat'
export default class Matricula {

  constructor(nome, idade, altura, plano) {
    this._data = new Date()
    this._nome = nome
    this._idade = idade
    this._altura = altura
    this._plano = plano
    Object.freeze(this)
  }

  get data() {
    return DateFormat.toString(this._data)
  }
  get nome() {
    return this._nome
  }
  get idade() {
    return this._idade
  }
  get altura() {
    return this._altura
  }
  get plano() {
    return this._plano
  }

}